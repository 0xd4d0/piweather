#!/usr/bin/env python
# -*- coding: utf8 -*-

import scrollphathd as sphd
import time
import requests
import json
import os
import datetime
import sys

MAIN_INT = 600 #10 minutes
SPHD_INT = 0.05
SPHD_LOOP = int(MAIN_INT//SPHD_INT)
URL = "https://api.openweathermap.org/data/2.5/weather?q="
LOCATION = ""
API_KEY = ""
BRIGHTNESS_VALUES = {
        '00': 0.1,
        '01': 0.1,
        '02': 0.1,
        '03': 0.1,
        '04': 0.1,
        '05': 0.15,
        '06': 0.15,
        '07': 0.2,
        '08': 0.2,
        '09': 0.25,
        '10': 0.3,
        '11': 0.4,
        '12': 0.5,
        '13': 0.5,
        '14': 0.5,
        '15': 0.5,
        '16': 0.4,
        '17': 0.3,
        '18': 0.25,
        '19': 0.2,
        '20': 0.2,
        '21': 0.15,
        '22': 0.1,
        '23': 0.1,
}

def getWeather(url):
        url = url.replace("\n","")
        response = requests.get(url)
        response.raise_for_status()
        json_data = json.loads(response.text)
        juice = { 
                "temp_max" : json_data["main"]["temp_max"],
                "temp_min" : json_data["main"]["temp_min"],
                "humidity" : json_data["main"]["humidity"],
                "wind" : json_data["wind"]["speed"]
        }
        return juice

def displayWeather(maxTemp, minTemp, rainProx, wind):
        sphd.clear()
        print('\nHere comes the data!')
        dataToPrint = "H:" + str(maxTemp) + " L:" + str(minTemp) + " Hmd:" + str(rainProx) + "% W:" + str(wind) + "m/s "
        sphd.write_string(dataToPrint)
        sphd.set_brightness(adjustBrightness())
        for i in range(SPHD_LOOP):
                sphd.show()
                sphd.scroll(1)
                time.sleep(SPHD_INT)

def adjustBrightness(): 
        currentHour = datetime.datetime.now().strftime("%H")
        for key,value in BRIGHTNESS_VALUES.items():
                if key==currentHour:
                        return value

def main():
        with open("/home/pi/scripts/weather_api", "r") as f:
                data = f.readlines()
                API_KEY = data[0]
                LOCATION = data[1]
        print('''
        getCurrentWeather.py - Looks up the current weather.

        Location: %s 

        Press Ctrl+C to exit!''' % (LOCATION))
        while True:
                data_set = getWeather(URL+LOCATION+"&APPID="+API_KEY+"&units=metric")
                #displayWeather(maxTemp, minTemp, rainProximity, wind)
                displayWeather(data_set["temp_max"], data_set["temp_min"], data_set["humidity"], data_set["wind"])
                time.sleep(MAIN_INT)


if __name__=="__main__":
        try:
                main()
        except KeyboardInterrupt:
                sphd.clear()
                print('\nExiting!')
                sys.exit()
