# Pi Weather

This tiny script fetches the weather data from the [open weather map](https://openweathermap.org/) API and displays it on the scroll phat hd that is mounted on a raspberry pi zero.
Data includes the maximum and minimum temperature, the humidity percentage and wind velocity.

### What you need

* [Raspberry Pi Zero W](https://www.raspberrypi.org/products/raspberry-pi-zero-w/)
* [Scroll Phat HD](https://thepihut.com/products/scroll-phat-hd) 
* A file that has the API key and your desired location 
* Some time

### What to keep in mind

* Adjust the location of the file that holds the info
* I only display data that is relevant to me
